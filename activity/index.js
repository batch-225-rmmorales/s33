//#3
async function numberThree() {
  console.log("#3");
  await fetch("https://jsonplaceholder.typicode.com/todos", { method: "GET" })
    .then((res) => res.text())
    .then((data) => console.log(data));
}

//#4
async function numberFour() {
  console.log("#4");
  let titles = [];
  response = await fetch("https://jsonplaceholder.typicode.com/todos");
  json = await response.json();
  json.map((todo) => titles.push(todo.title));

  console.log(titles);
}

//#5
async function fetchOne(x) {
  const response = await fetch(
    "https://jsonplaceholder.typicode.com/todos/" + x,
    {
      method: "GET",
    }
  );
  const json = await response.json();
  return json;
}
async function numberFive() {
  console.log("#5");

  data = await fetchOne(3);
  console.log(data);
}

//#6
async function numberSix() {
  console.log("#6");

  data = await fetchOne(3);
  console.log(`title is: ${data.title} and completed is ${data.completed}`);
}

//#7
async function numberSeven() {
  console.log("#7");
  const response = await fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "finish activity",
      completed: false,
      userId: 999,
      id: 999,
    }),
  });
  json = await response.json();
  console.log(json);
}

//#8
async function numberEight() {
  console.log("#8");
  const response = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Change Title",
    }),
  });
  json = await response.json();
  console.log(json);
}

//#9
async function numberNine() {
  console.log("#9");
  const response = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Change Title",
      description: "will this work",
      status: "active",
      date_completed: "today",
      userId: 999,
    }),
  });
  json = await response.json();
  console.log(json);
}
//#10
async function numberTen() {
  console.log("#10");
  const response = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Change Title",
    }),
  });
  json = await response.json();
  console.log(json);
}
//#11
async function numberEleven() {
  console.log("#11");
  const response = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      completed: true,
      date_completed: "today",
    }),
  });
  json = await response.json();
  console.log(json);
}

//#12

async function numberTwelve() {
  console.log("#12");
  const response = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
  });
  json = await response.json();
  console.log(json);
}

//print all answers
async function allAnswers() {
  await numberThree();
  await numberFour();
  await numberFive();
  await numberSix();
  await numberSeven();
  await numberEight();
  await numberNine();
  await numberTen();
  await numberEleven();
  await numberTwelve();
}
allAnswers();
