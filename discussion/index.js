// asynchronous vs synchronous
// java script by default is synchronous

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// fetch("https://jsonplaceholder.typicode.com/posts").then((response) => {
//   response.json().then((data) => console.log(data));
// });

console.log("hello");
console.log("goodbye");
let posts = "";
fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => {
    response.text().then((data) => {
      posts = JSON.parse(data);
      console.log(posts);
      console.log(posts[1]);
    });
  })
  .then((mors) => {
    console.log("printme");
    console.log(posts[2]);
  });

// getting a specific post
//fetch("https://jsonplaceholder.typicode.com/posts/2").
// let posts = "";
fetch("https://jsonplaceholder.typicode.com/posts").then((response) => {
  response.text().then((data) => {
    posts = JSON.parse(data);
    // console.log(posts);
    console.log(posts[1]);
  });
});

//creating a post
// fetch('URL',options).then((response)=>{}).then((response)=>{})
//create a new post
fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Post ni Mors",
    body: "HELLO AKO SI MORS",
    userId: 666,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// Sets the method of the "Request" object to "POST" following REST API
// Default method is GET
// Sets the header data of the "Request" object to be sent to the backend
// Specified that the content will be in a JSON structure
// Sets the content/body data of the "Request" object to be sent to the backend
// JSON.stringify converts the object data into a stringified JSON

//may patch pala aside from put and post
// use .map sa activity?

//https://jsonplaceholder.typicode.com/todos
